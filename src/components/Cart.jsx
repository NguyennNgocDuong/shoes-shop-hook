import React from "react";

export default function Cart({ cart, handleChangeQuantity }) {
  console.log("cart");
  let renderTbodyCart = () => {
    return cart.map((shoes) => {
      return (
        <tr>
          <td>{shoes.id}</td>
          <td>
            <img src={shoes.image} style={{ width: 150 }} alt="" />
          </td>
          <td>{shoes.name}</td>
          <td>${shoes.price}</td>
          <td>
            <button
              onClick={() => handleChangeQuantity(shoes.id, -1)}
              className="btn btn-success"
            >
              -
            </button>
            <span className="mx-3">{shoes.soLuong}</span>
            <button
              onClick={() => handleChangeQuantity(shoes.id, 1)}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>${shoes.price * shoes.soLuong}</td>
        </tr>
      );
    });
  };

  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Hình ảnh</th>
            <th>Tên</th>
            <th>Giá</th>
            <th>Số lượng</th>
            <th>Tổng tiền</th>
          </tr>
        </thead>
        <tbody>{renderTbodyCart()}</tbody>
      </table>
    </div>
  );
}
