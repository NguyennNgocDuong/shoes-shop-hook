import React, { useState } from "react";
import { data_shoes } from "../data/dataShoes";
import Cart from "./Cart";
import ListItem from "./ListItem";

export default function ShoesShop() {
  let [state, setState] = useState({ data_shoes, cart: [] });

  let handleChangeQuantity = (id, value) => {
    console.log("change quantity");
    let newCart = [...state.cart];

    let index = newCart.findIndex((item) => {
      return item.id === id;
    });
    if (index !== -1) {
      newCart[index].soLuong += value;
      newCart[index].soLuong < 1 && newCart.splice(index, 1);
    }
    setState({ data_shoes, cart: newCart });
  };

  let handleAddToCart = (shoes) => {
    console.log("add to cart");
    let newCart = [...state.cart];

    let index = newCart.findIndex((item) => {
      return item.id === shoes.id;
    });
    if (index == -1) {
      newCart.push({ ...shoes, soLuong: 1 });
    } else {
      newCart[index].soLuong++;
    }

    setState({ data_shoes, cart: newCart });
  };

  return (
    <div className="container">
      {state.cart.length > 0 ? (
        <Cart handleChangeQuantity={handleChangeQuantity} cart={state.cart} />
      ) : (
        <h2 className="text-danger">Bạn chưa có sản phẩm trong giỏ hàng!!!</h2>
      )}
      <ListItem handleClick={handleAddToCart} dataShoes={state.data_shoes} />
    </div>
  );
}
