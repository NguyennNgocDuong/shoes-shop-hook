import React from "react";

export default function ItemShoes({ shoes, handleAddToCart }) {
  console.log("itemShoes");
  return (
    <div className="col-3 my-3">
      <div
        className="card"
        style={{
          height: "100%",
          boxShadow:
            "rgba(0, 0, 0, 0.3) 0px 19px 38px, rgba(0, 0, 0, 0.22) 0px 15px 12px",
        }}
      >
        <img className="card-img-top" src={shoes.image} alt />
        <div className="card-body d-flex flex-column justify-content-between">
          <h4 className="card-title">{shoes.name}</h4>
          <p className="card-text">${shoes.price}</p>
          <button
            onClick={() => handleAddToCart(shoes)}
            className="btn btn-dark"
          >
            Thêm giỏ hàng
          </button>
        </div>
      </div>
    </div>
  );
}
