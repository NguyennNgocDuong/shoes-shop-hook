import React from "react";
import ItemShoes from "./ItemShoes";

export default function ListItem({ dataShoes, handleClick }) {
  console.log("ListItem");
  let renderListShoes = () => {
    return dataShoes.map((shoes) => {
      return (
        <ItemShoes handleAddToCart={handleClick} key={shoes.id} shoes={shoes} />
      );
    });
  };
  return <div className="row">{renderListShoes()}</div>;
}
